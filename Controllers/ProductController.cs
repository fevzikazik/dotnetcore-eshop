﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using onlineShopping.Data;
using onlineShopping.Service;

namespace onlineShopping.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        //product details service

        public ProductController(IProductService product)
        {
         _productService=product;
         //details   
        }
        // GET: api/products
        [HttpGet]
        public List<Product> GetResult(){
            return _productService.GetProducts().ToList();
        }

        [HttpPost]

        public IActionResult PostProduct(Product product){
            
            _productService.InsertProduct(product);
            return Ok();
        }
    }
}
