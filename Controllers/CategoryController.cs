using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using onlineShopping.Data;
using onlineShopping.Service;

namespace onlineShopping.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;

        public CategoryController(ICategoryService categoryService, IProductService productService)
        {
            _categoryService = categoryService;
            _productService = productService;
        }

        [HttpGet]
        // GET: api/category
        public List<Category> GetResult()
        {
            return _categoryService.GetCategories().ToList();
        }

        // GET: api/category/5
        [HttpGet("{id}")]
        public IEnumerable<Product> GetProductsByCategory(long id)
        {
            return _productService.GetProducts().Where(p => p.CBilmemne == id).ToList();
        }
        
        // Fevzi Editledi...
    }
}