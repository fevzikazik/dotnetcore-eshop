using onlineShopping.Data;
using Microsoft.EntityFrameworkCore;  
using System;  
using System.Collections.Generic;  
using System.Linq; 

namespace onlineShopping.Repo{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {

        private readonly ApplicationContext _context;  
        private DbSet<T> entities;

        public Repository(ApplicationContext context)
        {
            this._context = context;
            entities = context.Set<T>();
        }

        public void Delete(T entity)
        {
            if (entity == null)  
            {  
                throw new ArgumentNullException("entity");  
            }  
            entities.Remove(entity);  
            _context.SaveChanges();  
        }

        public T Get(long id)
        {
            return entities.SingleOrDefault(s => s.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable(); 
        }

        public void Insert(T entity)
        {
             if (entity == null)  
            {  
                throw new ArgumentNullException("entity");  
            }  
            entities.Add(entity);  
            _context.SaveChanges(); 
        }

        public void Remove(T entity)
        {
            if (entity == null)  
            {  
                throw new ArgumentNullException("entity");  
            }  
            entities.Remove(entity);   
        }

        public void SaveChanges()
        {
           _context.SaveChanges();  
        }

        public void Update(T entity)
        {
            if (entity == null)  
            {  
                throw new ArgumentNullException("entity");  
            }  
            _context.SaveChanges();  
        }
    }
}