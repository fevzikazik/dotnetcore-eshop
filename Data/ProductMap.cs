using Microsoft.EntityFrameworkCore.Metadata.Builders;  
namespace onlineShopping.Data{
    public class ProductMap{
        public ProductMap(EntityTypeBuilder<Product> entityBuilder)
        {
             entityBuilder.HasKey(t => t.Id);
             entityBuilder.Property(t=> t.Title).IsRequired();
             entityBuilder.Property(t=> t.Content).IsRequired();
             entityBuilder.Property(t=> t.CBilmemne).IsRequired();
             
        }
    }
}