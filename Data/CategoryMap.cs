using Microsoft.EntityFrameworkCore.Metadata.Builders;  

namespace onlineShopping.Data{
    public class CategoryMap{
        public CategoryMap(EntityTypeBuilder<Category> entityBuilder)
        {
            entityBuilder.HasKey(c=> c.Id);
            entityBuilder.Property(c=> c.Name).IsRequired();
        }
    }
}