using onlineShopping.Data;
using System.Collections.Generic;

namespace onlineShopping.Service
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetCategories();
        Category GetCategory(long id);
        void InsertCategory(Category category);
        void UpdateCategory(Category category);
        void DeleteCategory(long id);

    }

}