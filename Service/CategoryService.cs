using onlineShopping.Data;
using onlineShopping.Repo;
using System.Collections.Generic;

namespace onlineShopping.Service{
    public class CategoryService : ICategoryService
    {
        private IRepository<Category> categoryRepository;

        public CategoryService(IRepository<Category> categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public void DeleteCategory(long id)
        {
            Category category = GetCategory(id);
            categoryRepository.Remove(category);
            categoryRepository.SaveChanges();
        }

        public IEnumerable<Category> GetCategories()
        {
            return categoryRepository.GetAll();
        }

        public Category GetCategory(long id)
        {
            return categoryRepository.Get(id);
        }

        public void InsertCategory(Category category)
        {
            categoryRepository.Insert(category);
        }

        public void UpdateCategory(Category category)
        {
            categoryRepository.Update(category);
        }
    }
}