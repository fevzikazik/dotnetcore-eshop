using onlineShopping.Data;
using onlineShopping.Repo;
using System.Collections.Generic;

namespace onlineShopping.Service
{
    public class ProductService : IProductService
    {
        private IRepository<Product> productRepository;
        //Add more repo : prodcutdetails etc

        public ProductService(IRepository<Product> productRepository)
        {
            this.productRepository = productRepository;
        }

        public void DeleteProduct(long id)
        {
            Product product = GetProduct(id);
            productRepository.Remove(product);
            productRepository.SaveChanges();
        }

        public Product GetProduct(long id)
        {
            return productRepository.Get(id);
        }

        public IEnumerable<Product> GetProducts()
        {
            return productRepository.GetAll();
        }
        public IEnumerable<Product> GetProductsByCategory(Category cate)
        {
            return productRepository.GetAll();
        }
        public void InsertProduct(Product product)
        {
            productRepository.Insert(product);
        }

        public void UpdateProduct(Product product)
        {
           productRepository.Insert(product);
        }
    }
}
