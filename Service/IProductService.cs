using onlineShopping.Data;
using System.Collections.Generic;

namespace onlineShopping.Service
{
    public interface IProductService
    {
        IEnumerable<Product> GetProducts();
        Product GetProduct(long id);
        void InsertProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(long id);



    }
}